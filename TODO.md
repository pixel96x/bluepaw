# TODO

**NOTE:** this list is incomplete

## Search

* [ ] local/user blacklist
* [ ] tag suggestion/autocoplete

## Media

* [ ] support for DText formatting
* [ ] support for animated GIFs
* [ ] support for animated PNGs
* [ ] support for playing videos

## Input

* [ ] support for touch input
* [ ] support for stylus pens

## Preferences

* [x] light/dark/system default theme
* [x] saving size and/or window position

## e621.net accounts

* [ ] ability to log in
* [ ] using preferences and blacklist from account
* [ ] reciving/sending DMails

## UI/UX

* [ ] moving backward and forward (tab history)
* [ ] translations
* [ ] enchanced uploader

## Extra functionality

* [ ] tag subscription
  * [ ] background service/daemon checking for new posts
* [ ] mass downloader
  * [ ] ability to continue previous downloads after application exit

---

## Platforms

### OS

* [ ] version for Windows
  * [ ] x86_64
  * [ ] arm64
* [ ] version for macOS
  * [ ] x86_64
  * [ ] arm64
* [ ] version for Linux
  * [x] x86_64
  * [ ] arm64

### Distribution

#### Linux

* [ ] Flatpak version
* [ ] AppImage version
