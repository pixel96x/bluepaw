/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/search_result_card.ui")]
    public sealed class SearchResultCard : Adw.Bin
    {
        public uint64 id { get; construct; }

        [GtkChild]
        private unowned Gtk.Spinner spinner;
        [GtkChild]
        private unowned Gtk.Image icon_thumbnail;
        [GtkChild]
        private unowned Adw.Clamp thumbnail_clamp;
        [GtkChild]
        private unowned Gtk.Picture thumbnail;
        [GtkChild]
        private unowned Gtk.Label score;
        [GtkChild]
        private unowned Gtk.Label favcount;
        [GtkChild]
        private unowned Gtk.Label rating;

        private PageSearchResults page_search_results;


        public SearchResultCard(Posts.Post post,
                                PageSearchResults page_search_results)
        {
            Object(
                id: post.id
            );

            this.page_search_results = page_search_results;

            this.icon_thumbnail.height_request = this.thumbnail.height_request = (int) post.preview_height;
            this.icon_thumbnail.width_request = this.thumbnail.width_request = (int) post.preview_width;

            if (post.upvotes + post.downvotes > 0)
                this.score.label = "👍\u00A0" + (post.upvotes + post.downvotes).to_string();
            else if (post.upvotes + post.downvotes < 0)
                this.score.label = "👎\u00A0" + (post.upvotes + post.downvotes).to_string();
            else
                this.score.label = "0";

            this.favcount.label = "🤟\u00A0" + post.favcount.to_string();

            switch (post.rating) {
                case Posts.Rating.SAFE:
                    this.rating.label = "<span foreground=\"#57e389\" weight=\"bold\">S</span>";
                    this.rating.tooltip_text = "Rating: safe";
                    break;
                case Posts.Rating.QUESTIONABLE:
                    this.rating.label = "<span foreground=\"#f8e45c\" weight=\"bold\">Q</span>";
                    this.rating.tooltip_text = "Rating: questionable";
                    break;
                case Posts.Rating.EXPLICIT:
                    this.rating.label = "<span foreground=\"#ed333b\" weight=\"bold\">E</span>";
                    this.rating.tooltip_text = "Rating: explicit";
                    break;
            }

            var gest = new Gtk.GestureClick();
            gest.set_button(Gdk.BUTTON_PRIMARY);

            gest.pressed.connect((key, x, y) => {
                this.page_search_results.page_manager.load_post_page(post);
            });

            this.add_controller(gest);

            gest = new Gtk.GestureClick();
            gest.set_button(Gdk.BUTTON_MIDDLE);

            gest.pressed.connect((key, x, y) => {
                new PageManager.new_from_post(this.page_search_results.page_manager.tab_view, this.page_search_results.page_manager.page_controls_bin, post, false);
            });

            this.add_controller(gest);

            if (Posts.Flags.DELETED in post.flags) {
                this.spinner.spinning = false;
                this.spinner.visible = false;
                this.icon_thumbnail.icon_name = "user-trash-symbolic";
                this.icon_thumbnail.tooltip_text = "Post deleted";
                this.icon_thumbnail.visible = true;
                return;
            }

            if (post.preview_url == null) {
                this.spinner.spinning = false;
                this.spinner.visible = false;
                this.icon_thumbnail.tooltip_text = "Preview unavailable";
                this.icon_thumbnail.visible = true;
                return;
            }

            Network.fetch_image.begin(post.preview_url, null, Network.CallType.NET, (obj, res) => {
                var pb = Network.fetch_image.end(res);

                this.spinner.spinning = false;
                this.spinner.visible = false;

                if (pb == null) {
                    this.icon_thumbnail.visible = true;
                    return;
                }

                this.thumbnail_clamp.maximum_size = (int) post.preview_width;
                this.thumbnail.set_pixbuf(pb);
                this.thumbnail.visible = true;
            });
        }
    }
}
