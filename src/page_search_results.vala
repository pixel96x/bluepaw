/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/page_search_results.ui")]
    public sealed class PageSearchResults : Adw.Bin, IPage
    {
        public Icon icon { get; construct; }
        public string last_search_query
        {
            get { return this.search_entry.buffer.text; }
            construct set { this.search_entry.text = value; }
        }
        public PageManager page_manager { get; construct; }
        public BluePaw.PageType page_type { get; construct; }
        public string title { get; protected set; }

        [GtkChild]
        private unowned Gtk.Entry search_entry;
        [GtkChild]
        private unowned Gtk.Button search_btn;
        [GtkChild]
        private unowned Adw.Clamp spinner_clamp;
        [GtkChild]
        private unowned Gtk.Spinner spinner;
        [GtkChild]
        private unowned Adw.StatusPage status_page;
        [GtkChild]
        private unowned Gtk.FlowBox results_flow_box;

        private Posts.Post[]? posts;

        public PageSearchResults(PageManager page_manager,
                                 Posts.Post[]? posts_list,
                                 string? search_query = "")
        {
            Object(
                icon: new ThemedIcon("view-grid-symbolic"),
                last_search_query: search_query,
                page_manager: page_manager,
                page_type: PageType.SEARCH_RESULTS,
                title: "Search results"
            );

            this.posts = posts_list;
            this.spinner.spinning = false;
            this.spinner_clamp.visible = false;

            if (posts == null) {
                this.status_page.visible = true;
                this.results_flow_box.visible = false;
            } else {
                this.status_page.visible = false;
                this.results_flow_box.visible = true;

                foreach (var post in posts)
                    this.results_flow_box.append(new SearchResultCard(post, this));
            }
        }

        public PageSearchResults.from_tags(string? tags = null,
                                           string? search_query = null)
        {
            Object(
                icon: new ThemedIcon("view-grid-symbolic"),
                last_search_query: search_query,
                page_type: PageType.SEARCH_RESULTS,
                title: "Search results"
            );

            this.spinner.spinning = true;
            this.spinner_clamp.visible = true;

            Posts.fetch_posts.begin("https://e621.net/posts.json?=tags" + tags, null, (obj, res) => {
                this.posts = Posts.fetch_posts.end(res);
                this.spinner.spinning = false;
                this.spinner_clamp.visible = false;

                if (posts == null) {
                    this.status_page.visible = true;
                    this.results_flow_box.visible = false;
                } else {
                    this.status_page.visible = false;
                    this.results_flow_box.visible = true;

                    foreach (var post in posts)
                        this.results_flow_box.append(new SearchResultCard(post, this));
                }
            });
        }

        [GtkCallback]
        private void search_entry_activate_cb(Gtk.Entry entry)
        {
            this.search_btn.clicked();
        }

        [GtkCallback]
        private void search_btn_clicked_cb(Gtk.Button button)
        {
            this.search_entry.sensitive = this.search_btn.sensitive = false;
            this.status_page.visible = false;
            this.results_flow_box.visible = false;
            this.spinner.spinning = true;
            this.spinner_clamp.visible = true;

            Posts.fetch_posts.begin("https://e621.net/posts.json?tags=" + this.search_entry.buffer.text, null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.search_entry.sensitive = this.search_btn.sensitive = true;
                this.spinner.spinning = false;
                this.spinner_clamp.visible = false;

                if (posts == null) {
                    this.status_page.visible = true;
                    return;
                }

                Gtk.Widget child;
                while ((child = this.results_flow_box.get_first_child()) != null)
                    this.results_flow_box.remove(child);

                this.results_flow_box.visible = true;

                foreach (var post in posts)
                    this.results_flow_box.append(new SearchResultCard(post, this));
            });
        }
    }
}
