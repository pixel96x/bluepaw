/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;
using Soup;

namespace BluePaw
{
    namespace Network
    {
        public enum CallType
        {
            NET,
            API
        }

        public Session net_session;
        public Session api_session;

        public static async uint8[]? fetch_data(string url,
                                                Cancellable? cancel = null,
                                                CallType call_type = CallType.NET)
        {
            uint8[] data = new uint8[0];
            uint8[] buffer = new uint8[4096];
            size_t bytes_read;
            InputStream inputS = null;
            Soup.Message msg = new Soup.Message("GET", url);

            switch (call_type) {
                case CallType.NET:
                    try {
                        inputS = yield net_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
                case CallType.API:
                    try {
                        inputS = yield api_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
            }

            if (msg.status_code != 200) {
                printerr("Error: unsuccessfull response: %u\n", msg.status_code);
                return null;
            }

            try {
                while (yield inputS.read_all_async(buffer, Priority.DEFAULT, cancel, out bytes_read)) {
                    if (bytes_read < 1)
                        break;

                    data.resize(data.length + (int) bytes_read);
                    for (int i = 0; i < (int) bytes_read; ++i)
                        data[(int) data.length - (int) bytes_read + i] = buffer[i];
                }
            } catch (Error error) {
                printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                return null;
            }

            return data.length > 0 ? data : null;
        }

        public static async Gdk.Pixbuf? fetch_image(string url,
                                                    Cancellable? cancel = null,
                                                    CallType call_type = CallType.NET)
        {
            Gdk.Pixbuf img;
            InputStream inputS = null;
            Soup.Message msg = new Soup.Message("GET", url);

            switch (call_type) {
                case CallType.NET:
                    try {
                        inputS = yield net_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
                case CallType.API:
                    try {
                        inputS = yield api_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
            }

            if (msg.status_code != 200) {
                printerr("Error: unsuccessfull response: %u\n", msg.status_code);
                return null;
            }

            try {
                img = yield new Gdk.Pixbuf.from_stream_async(inputS, cancel);
            } catch (Error error) {
                printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                return null;
            }

            return img;
        }

        public static async Gdk.PixbufAnimation? fetch_animation(string url,
                                                                 Cancellable? cancel = null,
                                                                 CallType call_type = CallType.NET)
        {
            Gdk.PixbufAnimation animation;
            InputStream inputS = null;
            Soup.Message msg = new Soup.Message("GET", url);

            switch (call_type) {
                case CallType.NET:
                    try {
                        inputS = yield net_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
                case CallType.API:
                    try {
                        inputS = yield api_session.send_async(msg, Priority.DEFAULT, cancel);
                    } catch (Error error) {
                        printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                        return null;
                    }
                    break;
            }

            if (msg.status_code != 200) {
                printerr("Error: unsuccessfull response: %u\n", msg.status_code);
                return null;
            }

            try {
                animation = yield new Gdk.PixbufAnimation.from_stream_async(inputS, cancel);
            } catch (Error error) {
                printerr("Error: [%s]: %d: %s\n", error.domain.to_string(), error.code, error.message);
                return null;
            }

            return animation;
        }
    }
}
