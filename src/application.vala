/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    public sealed class Application : Adw.Application
    {
        public static Preferences preferences { get; protected set; }
        public static State state { get; protected set; }
        public static File tmp_dir { get; protected set; }

        private Window main_window;
        private Adw.PreferencesWindow preferences_window;
        private Gtk.ShortcutsWindow shortcuts_dialog;
        private Gtk.AboutDialog about_dialog;

        public Application()
        {
            Object(
                application_id: "com.pixel96x.BluePaw",
                flags: ApplicationFlags.FLAGS_NONE
            );
        }

        public override void startup()
        {
            ActionEntry[] actionEntries = {
                {"preferences", preferences_cb, null, null, null},
                {"keyboard-shortcuts", keyboard_shortcuts_cb, null, null, null},
                {"about", about_action_cb, null, null, null},
                {"quit", quit_action_cb, null, null, null}
            };

            base.startup();

            Network.api_session = new Soup.Session.with_options("user-agent", "bluepaw v0.2.0 (by pixel96x)", "max-conns", 2);
            Network.net_session = new Soup.Session.with_options("user-agent", "bluepaw v0.2.0 (by pixel96x)", "max-conns", 10);

            if (Application.preferences == null)
                Application.preferences = new Preferences();

            if (Application.state == null)
                Application.state = new State();

            if (Application.tmp_dir == null) {
                try {
                    Application.tmp_dir = File.new_for_path(DirUtils.make_tmp("bluepaw_XXXXXX"));
                } catch (FileError err) {
                    printerr("Error: [%s]: %d: %s\n", err.domain.to_string(), err.code, err.message);
                    this.quit();
                }
            }

            this.add_action_entries(actionEntries, this);
            this.set_accels_for_action("app.preferences", {"<ctrl>comma"});
            this.set_accels_for_action("app.keyboard-shortcuts", {"<ctrl>question"});
            this.set_accels_for_action("app.quit", {"<ctrl>q"});

            Gtk.Window.set_default_icon_name("application-x-executable");

            Unix.signal_add(Posix.Signal.HUP, (() => {
                this.prepare_shutdown();
                return Source.REMOVE;
            }), Priority.HIGH);

            Unix.signal_add(Posix.Signal.INT, (() => {
                this.prepare_shutdown();
                return Source.REMOVE;
            }), Priority.HIGH);

            Unix.signal_add(Posix.Signal.TERM, (() => {
                this.prepare_shutdown();
                return Source.REMOVE;
            }), Priority.HIGH);

            this.menubar = new Gtk.Builder.from_resource("/com/pixel96x/BluePaw/ui/menubar.ui").get_object("menubar") as MenuModel;
        }

        public override void activate()
        {
            if (this.main_window == null) {
                this.main_window = new Window(this);

                this.preferences_window = new PreferencesWindow(this.main_window);

                this.shortcuts_dialog = new Gtk.Builder.from_resource("/com/pixel96x/BluePaw/ui/shortcuts_dialog.ui").get_object("dialog")
                                   as Gtk.ShortcutsWindow;
                this.shortcuts_dialog.application = this;
                this.shortcuts_dialog.transient_for = main_window;

                this.about_dialog = new Gtk.Builder.from_resource("/com/pixel96x/BluePaw/ui/about_dialog.ui").get_object("dialog")
                               as Gtk.AboutDialog;
                this.about_dialog.application = this;
                this.about_dialog.transient_for = main_window;

                main_window.show();
            } else {
                main_window.present();
            }
        }

        public override void shutdown()
        {
            clean_tmp_dir(Application.tmp_dir);

            base.shutdown();
        }

        // NOTE: "force" parameter is not currently used
        //       it will be intended to forcefully close up app, even with running tasks
        public void prepare_shutdown(bool force = false)
        {
            if (Application.preferences.window.get_boolean("remember-size")) {
                int width, height;
                this.main_window.get_default_size(out width, out height);
                Application.state.window.set("size", "(ii)", width, height);
                Application.state.window.set_boolean("maximized", this.main_window.maximized);
            }

            Application.state.window.apply();

            this.quit();
        }

        private void preferences_cb()
        {
            this.preferences_window.show();
        }

        private void keyboard_shortcuts_cb()
        {
            this.shortcuts_dialog.show();
        }

        private void about_action_cb()
        {
            this.about_dialog.show();
        }

        private void quit_action_cb()
        {
            this.prepare_shutdown();
        }

        private void clean_tmp_dir(File file)
        {
            Dir dir;
            File dirFile;
            string? file_name;

            if (!file.query_exists(null))
                return;

            try {
                dir = Dir.open(file.get_path());

                while ((file_name = dir.read_name()) != null) {
                    dirFile = File.new_for_path(file.get_path() + "/" + file_name);

                    if (dirFile.query_file_type(FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null) == FileType.DIRECTORY)
                        clean_tmp_dir(dirFile);

                    if (dirFile.query_exists(null))
                        dirFile.delete(null);
                }

                if (file.query_exists(null))
                    file.delete(null);
            } catch (FileError err) {
                printerr("Error: [%s]: %d: %s\n", err.domain.to_string(), err.code, err.message);
                return;
            } catch (Error err) {
                printerr("Error: [%s]: %d: %s\n", err.domain.to_string(), err.code, err.message);
                return;
            }
        }
    }
}
