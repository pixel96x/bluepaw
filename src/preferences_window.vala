/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/preferences_window.ui")]
    public sealed class PreferencesWindow : Adw.PreferencesWindow
    {
        // widgets - general
        [GtkChild]
        private unowned Adw.ExpanderRow general_theme_expander_row;
        [GtkChild]
        private unowned Adw.ActionRow  general_theme_default_row;
        [GtkChild]
        private unowned Adw.ActionRow  general_theme_light_row;
        [GtkChild]
        private unowned Adw.ActionRow  general_theme_dark_row;
        [GtkChild]
        private unowned Gtk.CheckButton general_theme_default_check;
        [GtkChild]
        private unowned Gtk.CheckButton general_theme_light_check;
        [GtkChild]
        private unowned Gtk.CheckButton general_theme_dark_check;
        // widgets - window
        [GtkChild]
        private unowned Gtk.Switch window_rem_size_sw;

        public PreferencesWindow(Window? parent_window = null)
        {
            Object(
                application: parent_window.application,
                transient_for: parent_window
            );

            // reading saved preferences
            switch (Application.preferences.general.get_enum("theme")) {
                case (0):
                    this.general_theme_expander_row.subtitle = this.general_theme_default_row.title;
                    this.general_theme_default_check.active = true;
                    break;
                case (1):
                    this.general_theme_expander_row.subtitle = this.general_theme_light_row.title;
                    this.general_theme_light_check.active = true;
                    break;
                case (2):
                    this.general_theme_expander_row.subtitle = this.general_theme_dark_row.title;
                    this.general_theme_dark_check.active = true;
                    break;
            }

            this.window_rem_size_sw.active = Application.preferences.window.get_boolean("remember-size");

            // setting up signals
            Application.preferences.general.changed["theme"].connect((prefs, key) => {
                switch (Application.preferences.general.get_enum("theme")) {
                    case (0):
                        this.general_theme_expander_row.subtitle = this.general_theme_default_row.title;
                        this.general_theme_default_check.active = true;
                        break;
                    case (1):
                        this.general_theme_expander_row.subtitle = this.general_theme_light_row.title;
                        this.general_theme_light_check.active = true;
                        break;
                    case (2):
                        this.general_theme_expander_row.subtitle = this.general_theme_dark_row.title;
                        this.general_theme_dark_check.active = true;
                        break;
                }
            });

            Application.preferences.window.changed["remember-size"].connect((prefs, key) => {
                this.window_rem_size_sw.active = Application.preferences.window.get_boolean("remember-size");
            });
        }

        [GtkCallback]
        private void general_theme_default_check_cb(Gtk.CheckButton button)
        {
            Application.preferences.general.set_enum("theme", 0);
        }

        [GtkCallback]
        private void general_theme_default_check_toggled_cb(Gtk.CheckButton button)
        {
            general_theme_default_check_cb(button);
        }

        [GtkCallback]
        private void general_theme_light_check_cb(Gtk.CheckButton button)
        {
            Application.preferences.general.set_enum("theme", 1);
        }

        [GtkCallback]
        private void general_theme_light_check_toggled_cb(Gtk.CheckButton button)
        {
            general_theme_light_check_cb(button);
        }

        [GtkCallback]
        private void general_theme_dark_check_cb(Gtk.CheckButton button)
        {
            Application.preferences.general.set_enum("theme", 2);
        }

        [GtkCallback]
        private void general_theme_dark_check_toggled_cb(Gtk.CheckButton button)
        {
            general_theme_dark_check_cb(button);
        }

        [GtkCallback]
        private void window_rem_size_sw_active_cb(Object obj, ParamSpec param)
        {
            Application.preferences.window.set_boolean("remember-size", this.window_rem_size_sw.active);
        }
    }
}
