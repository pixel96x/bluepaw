/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/page_post_view.ui")]
    public sealed class PagePostView : Adw.Bin, IPage
    {
        public Icon icon { get; construct; }
        public PageManager page_manager { get; construct; }
        public PageType page_type { get; construct; }
        public IPage prev_page { get; construct; }
        public string title { get; protected set; }

        [GtkChild]
        private unowned Gtk.Spinner loading_spinner;
        [GtkChild]
        private unowned Gtk.Image error_icon;

        [GtkChild]
        private unowned Adw.Bin invalid_tags_card;
        [GtkChild]
        private unowned Gtk.Box invalid_tags_box;
        [GtkChild]
        private unowned Adw.Bin artist_tags_card;
        [GtkChild]
        private unowned Gtk.Box artist_tags_box;
        [GtkChild]
        private unowned Adw.Bin copyright_tags_card;
        [GtkChild]
        private unowned Gtk.Box copyright_tags_box;
        [GtkChild]
        private unowned Adw.Bin character_tags_card;
        [GtkChild]
        private unowned Gtk.Box character_tags_box;
        [GtkChild]
        private unowned Adw.Bin species_tags_card;
        [GtkChild]
        private unowned Gtk.Box species_tags_box;
        [GtkChild]
        private unowned Adw.Bin general_tags_card;
        [GtkChild]
        private unowned Gtk.Box general_tags_box;
        [GtkChild]
        private unowned Adw.Bin lore_tags_card;
        [GtkChild]
        private unowned Gtk.Box lore_tags_box;
        [GtkChild]
        private unowned Adw.Bin meta_tags_card;
        [GtkChild]
        private unowned Gtk.Box meta_tags_box;
        [GtkChild]
        private unowned Gtk.Box informations_box;

        [GtkChild]
        private unowned Gtk.Box warning_box;
        [GtkChild]
        private unowned Gtk.Label warning_label;
        [GtkChild]
        private unowned Gtk.Separator warning_separator;

        [GtkChild]
        private unowned Adw.Clamp image_clamp;

        [GtkChild]
        private unowned Gtk.Label upvotes;
        [GtkChild]
        private unowned Gtk.Label downvotes;
        [GtkChild]
        private unowned Gtk.Label favcount;
        [GtkChild]
        private unowned Gtk.LevelBar score_levelbar;

        public PagePostView(PageManager page_manager,
                            Posts.Post post,
                            IPage? prev_page = null)
        {
            Object(
                icon: new ThemedIcon("image-x-generic-symbolic"),
                page_manager: page_manager,
                page_type: PageType.POST_VIEW,
                prev_page: prev_page,
                title: "Post #" + post.id.to_string()
            );

            this.image_clamp.child = this.loading_spinner;

            add_tags_to_card(this.invalid_tags_card, this.invalid_tags_box, post.invalid_tags);
            add_tags_to_card(this.artist_tags_card, this.artist_tags_box, post.artist_tags);
            add_tags_to_card(this.copyright_tags_card, this.copyright_tags_box, post.copyright_tags);
            add_tags_to_card(this.character_tags_card, this.character_tags_box, post.character_tags);
            add_tags_to_card(this.species_tags_card, this.species_tags_box, post.species_tags);
            add_tags_to_card(this.general_tags_card, this.general_tags_box, post.general_tags);
            add_tags_to_card(this.lore_tags_card, this.lore_tags_box, post.lore_tags);
            add_tags_to_card(this.meta_tags_card, this.meta_tags_box, post.meta_tags);
            add_informations_to_card(post);

            this.upvotes.label = "👍\u00A0" + post.upvotes.to_string();
            this.downvotes.label = "👎\u00A0" + post.downvotes.to_string();

            if (post.upvotes == 0 && post.downvotes == 0) {
                this.score_levelbar.value = 0.0;
                this.score_levelbar.tooltip_text = "No votes yet";
            } else {
                this.score_levelbar.value = (double) (post.upvotes / (double) (post.upvotes - post.downvotes));
                this.score_levelbar.tooltip_text = "Upvote: %.1f %%".printf(this.score_levelbar.value * 100.0);
            }

            this.favcount.label = "🤟\u00A0" + post.favcount.to_string();

            if (Posts.Flags.DELETED in post.flags) {
                this.loading_spinner.spinning = false;
                this.error_icon.icon_name = "user-trash-symbolic";
                this.error_icon.tooltip_text = "Post deleted";
                this.image_clamp.child = this.error_icon;
                return;
            }

            if (post.file_url == null) {
                this.loading_spinner.spinning = false;
                this.error_icon.tooltip_text = "File unavailable";
                this.image_clamp.child = this.error_icon;
                return;
            }

            switch (post.file_ext) {
                case Posts.FileExt.PNG:
                case Posts.FileExt.GIF:
                    foreach (var tag in post.meta_tags) {
                        if (tag == "animated") {
                            this.warning_label.label = "Animated GIFs/PNGs are not supported yet";
                            this.warning_box.visible = true;
                            this.warning_separator.visible = true;
                            break;
                        }
                    }

                    Network.fetch_image.begin(post.file_url, null, Network.CallType.NET, (obj, res) => {
                        var pb = Network.fetch_image.end(res);
                        this.loading_spinner.spinning = false;

                        if (pb == null) {
                            this.image_clamp.child = this.error_icon;
                            return;
                        }

                        this.image_clamp.maximum_size = (int) post.file_width;

                        var image = new Gtk.Picture.for_pixbuf(pb);
                        image.can_shrink = true;
                        image.height_request = 200;
                        image.keep_aspect_ratio = true;
                        image.width_request = 200;
                        this.image_clamp.child = image;
                    });
                    break;
                case Posts.FileExt.WEBM:
                    this.warning_label.label = "Videos are not supported yet";
                    this.warning_box.visible = true;
                    this.warning_separator.visible = true;

                    var btn = new Gtk.Button.with_label("Open in web browser");
                    btn.add_css_class("pill");
                    this.image_clamp.child = btn;

                    btn.clicked.connect((button) => {
                        Gtk.show_uri(null, "https://e621.net/posts/" + post.id.to_string(), Gdk.CURRENT_TIME);
                    });
                    break;
                case Posts.FileExt.SWF:
                    this.warning_label.label = "Flash Player is not supported";
                    this.warning_box.visible = true;
                    this.warning_separator.visible = true;

                    var btn = new Gtk.Button.with_label("Open in web browser");
                    btn.add_css_class("pill");
                    this.image_clamp.child = btn;

                    btn.clicked.connect((button) => {
                        Gtk.show_uri(null, "https://e621.net/posts/" + post.id.to_string(), Gdk.CURRENT_TIME);
                    });
                    break;
                default:
                    Network.fetch_image.begin(post.file_url, null, Network.CallType.NET, (obj, res) => {
                        var pb = Network.fetch_image.end(res);
                        this.loading_spinner.spinning = false;

                        if (pb == null) {
                            this.image_clamp.child = error_icon;
                            return;
                        }

                        this.image_clamp.maximum_size = (int) post.file_width;

                        var image = new Gtk.Picture.for_pixbuf(pb);
                        image.can_shrink = true;
                        image.height_request = 200;
                        image.keep_aspect_ratio = true;
                        image.width_request = 200;
                        this.image_clamp.child = image;
                    });
                    break;
            }
        }

        private void add_tags_to_card(Adw.Bin card,
                                      Gtk.Box box,
                                      string[]? tags)
        {
            if (tags.length > 0) {
                foreach (var tag in tags) {
                    var label = new Gtk.Label(tag.replace("_", " "));
                    label.halign = Gtk.Align.START;
                    label.wrap = true;
                    label.wrap_mode = Pango.WrapMode.WORD_CHAR;
                    box.append(label);
                }
            } else {
                card.visible = false;
            }
        }

        private void add_informations_to_card(Posts.Post post)
        {
            var label = new Gtk.Label("<b>ID:</b> " + post.id.to_string());
            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            label = new Gtk.Label("<b>Posted:</b> " + post.created_at.format("%x %X"));
            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            switch (post.rating) {
                case Posts.Rating.SAFE:
                    label = new Gtk.Label("<b>Rating:</b> <span foreground=\"#57e389\">safe</span>");
                    break;
                case Posts.Rating.QUESTIONABLE:
                    label = new Gtk.Label("<b>Rating:</b> <span foreground=\"#f8e45c\">questionable</span>");
                    break;
                case Posts.Rating.EXPLICIT:
                    label = new Gtk.Label("<b>Rating:</b> <span foreground=\"#ed333b\">explicit</span>");
                    break;
            }

            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            label = new Gtk.Label("<b>Resolution:</b> " + post.file_width.to_string() + "\u00A0\u00D7\u00A0" + post.file_height.to_string());
            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            label = new Gtk.Label("<b>Size:</b> " + format_size(post.file_size, FormatSizeFlags.DEFAULT));
            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            switch (post.file_ext) {
                case (Posts.FileExt.JPG):
                    label = new Gtk.Label("<b>Filetype:</b> jpg");
                    break;
                case (Posts.FileExt.PNG):
                    label = new Gtk.Label("<b>Filetype:</b> png");
                    break;
                case (Posts.FileExt.GIF):
                    label = new Gtk.Label("<b>Filetype:</b> gif");
                    break;
                case (Posts.FileExt.WEBM):
                    label = new Gtk.Label("<b>Filetype:</b> webm");
                    break;
                case (Posts.FileExt.SWF):
                    label = new Gtk.Label("<b>Filetype:</b> swf");
                    break;
            }
            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);

            if (Posts.Flags.PENDING in post.flags)
                label = new Gtk.Label("<b>Status:</b> pending");
            else if (Posts.Flags.FLAGGED in post.flags)
                label = new Gtk.Label("<b>Status:</b> flagged");
            else if (Posts.Flags.DELETED in post.flags)
                label = new Gtk.Label("<b>Status:</b> deleted");
            else
                label = new Gtk.Label("<b>Status:</b> active");

            label.halign = Gtk.Align.START;
            label.use_markup = true;
            this.informations_box.append(label);
        }
    }
}
